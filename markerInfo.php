<?php
/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
*/
	if(isset($_GET['station']))//Checking if Email Id and Password are set
	{
		$xml = new DOMDocument("1.0");
		$filename = '../../data/weatherMap.xml';//Relative Path to the XMl file
		$station = $_GET["station"];
		
		if (file_exists($filename)) //Checking if the XML file exists
		{
			$xml->load($filename);//Loading the XML file
			$searchNode = $xml->getElementsByTagName( "marker" ); 
			foreach( $searchNode as $val ) 
			{ 
				$stationtext = $val->getAttribute('name'); 
				if(strcmp($station,$stationtext) == 0)
				{
					echo "<strong>".$stationtext."</strong></br>";
					
					if($val->getElementsByTagName( "maxTemperature" )->length != 0)
					{					
						$maxtext = $val->getElementsByTagName( "maxTemperature" )->item(0)->nodeValue; 
						echo "<strong>Max Temperature : </strong>".$maxtext."</br>";
					}	
					if($val->getElementsByTagName( "minTemperature" )->length != 0)
					{					
						$mintext = $val->getElementsByTagName( "minTemperature" )->item(0)->nodeValue; 
						echo "<strong>Min Temperature : </strong>".$mintext."</br>";
					}
					if($val->getElementsByTagName( "rainTo9am" )->length != 0)
					{					
						$raintext = $val->getElementsByTagName( "rainTo9am" )->item(0)->nodeValue; 
						echo "<strong>Rain : </strong>".$raintext."</br>";
					}
					if($val->getElementsByTagName( "windRun" )->length != 0)
					{					
						$windtext = $val->getElementsByTagName( "windRun" )->item(0)->nodeValue; 
						echo "<strong>Wind Run : </strong>".$windtext."</br>";
					}
					if($val->getElementsByTagName( "sunshine" )->length != 0)
					{					
						$suntext = $val->getElementsByTagName( "sunshine" )->item(0)->nodeValue; 
						echo "<strong>Sunshine : </strong>".$suntext."</br>";
					}
				}
			}  			
		}	
	}
?>
