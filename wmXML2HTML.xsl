<?xml version="1.0" encoding="UTF-8"?>
<!-- Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<!-- Making the Caption and formating the date and time xml file -->
  <p><strong>Caption :</strong> <xsl:value-of select="markers/@name"/> <strong> Date:</strong>
  
	<xsl:variable name="myVar" select="markers/@create-time-local"/>
	<varoutput>
		<xsl:variable name="yy">
		   <xsl:value-of select="substring($myVar,1,4)" />
		</xsl:variable>
		<xsl:variable name="mm">
			<xsl:value-of select="substring($myVar,5,2)" />
		</xsl:variable>
		<xsl:variable name="dd">
			<xsl:value-of select="substring($myVar,7,2)" />
		</xsl:variable>
		<xsl:variable name="hh">
			<xsl:value-of select="substring($myVar,10,2)" />
		</xsl:variable>
		<xsl:variable name="min">
			<xsl:value-of select="substring($myVar,12,2)" />
		</xsl:variable>
		<xsl:value-of select="$yy"/>/<xsl:value-of select="$mm"/>/<xsl:value-of select="$dd"/>
		<strong> Local Time:</strong> <xsl:value-of select="$hh"/>:<xsl:value-of select="$min"/>
	</varoutput>
 </p>
 <!-- Making table of data from weatherMap.xml -->
  <table border="1">
    <tr>
      <th>Stations</th>
      <th>Max Temperature</th>
      <th>Min Temperature</th>
      <th>Rain To 9am (mm)</th>
      <th>Wind run (km/hr)</th>
      <th>Sunshine (hr)</th>
    </tr>
    <xsl:for-each select="markers/marker">
    <tr>
	<!-- Making an anchor link with onclick function of weather stations  -->
      <td><a href="#"> 
			<xsl:attribute name = "onclick">
				<xsl:text>showMap('</xsl:text>
				<xsl:value-of select = "@name"/>
				<xsl:text>')</xsl:text>
			</xsl:attribute>	
			<xsl:value-of select="@name"/>
		</a></td>
      <td><xsl:value-of select="maxTemperature"/></td>
      <td><xsl:value-of select="minTemperature"/></td>
      <td><xsl:value-of select="rainTo9am"/></td>
      <td><xsl:value-of select="windRun"/></td>
      <td><xsl:value-of select="sunshine"/></td>
    </tr>
    </xsl:for-each>
  </table>
  
</xsl:template>

</xsl:stylesheet>