<?php

/*Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
	This file will make the weatherMap.xml if not exist or will update in the existing weatherMap.xml
*/
	$doc = new DOMDocument("1.0");
	//making the url for xml file on australian weather site
	$statefile = $_GET["state"];
	$file = "ftp://ftp2.bom.gov.au/anon/gen/fwo/".$statefile;
	if (file_exists($file)) //Checking if the XML file exists on server
	{
		$doc = simplexml_load_file($file);
		
		$xsl = new DOMDocument;
		$xsl->load('wmXML2XML.xsl');

		// Configure the transformer
		$proc = new XSLTProcessor;
		$proc->importStyleSheet($xsl); // attach the xsl rules
		
		$filename = '../../data/weatherMap.xml';	//Relative Path to the XMl file
		$xml = new DOMDocument("1.0");
		$xml ->loadXML($proc->transformToXML($doc));
		//Saving the weatherMap.xml
		$strXml = $xml->saveXML() ;
		$xml->save($filename) or die("Error");
 
        echo $strXml;
	}
	//if file is not on australian weather site
	else
	{
		echo "NOTFOUND" ;
	}
	
?>
