# README #

# Weather Map Mashup #
Weather map mashup is developed using core Ajax Technlogies(Javascript/HTML, DOM, XML, XMLHttpRequest and XPath/XSL) and PHP veloped using PHP and MySQl technology. It is a website that will get current weather data in the form of an XML data file from the Bureau of Meteorology website for an Australian state. The data contained in the data file will be used with Google Maps API to create a mashup map with markers that contain weather information for towns (stations) in the selected state. The mashup includes six files named:-


* weatherMap.htm – This is the  html file that is the homepage for weather mashup system. User will select the state using the drop down here and will click the button to either show all the stations in map or show the stations data in a tabular format.
 
* wmXML2XML.xsl – This file will transform the Xml file from Australian Bureau website into weatherMap.xml. 
* weatherMap.xml – This file will contain all the stations information  with max temperature , min temperature, rain, windrun and sunshine.
* wmXML2HTML.xsl – This file will transform the weatherMap.xml file into html Tabular format showing in the div.
* weather.php – This file will be used for server side transformation on the   BOM xml file using wmXML2XML.xsl file . The result will be stored in wetherMap.xml file
* markerInfo.php – This file is used to get the content to be shown in the information window respectively to the clicked station.
 
* weather.js – This javascript file is used to display the table by performing client side transformation. It is also used to show the map with marker and the information window. All the station are showed using a function showStaions.
 