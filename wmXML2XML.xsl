<!-- Name-Dikshant Bawa 
	Student ID- 4942892
	Tutorial - Monday 19:30-20:30PM
	Tutor's Name - Tarique Anwar
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<!-- Making the weatherMap.xml file -->
<xsl:template match="weather-observations/product">
    <markers name="{@name}" create-time-local="{@create-time-local}">
	<!-- For each obs add a node as marker in weatherMap.xml -->
        <xsl:for-each select="group/obs">
            <marker name="{@station}">
			<!-- Making nodes of xml file which exists on australian weather site -->
				<xsl:for-each select="d">
					<xsl:choose>
						<xsl:when test="@t='tx'">
							<xsl:element name="maxTemperature">
								<xsl:value-of select="." />
							</xsl:element>		
						</xsl:when>	
						<xsl:when test="@t='tn'">
							<xsl:element name="minTemperature">
								<xsl:value-of select="." />
							</xsl:element>		
						</xsl:when>
						<xsl:when test="@t='r'">
							<xsl:element name="rainTo9am">
								<xsl:value-of select="." />
							</xsl:element>		
						</xsl:when>
						<xsl:when test="@t='wr'">
							<xsl:element name="windRun">
								<xsl:value-of select="." />
							</xsl:element>		
						</xsl:when>
						<xsl:when test="@t='sn'">
							<xsl:element name="sunshine">
								<xsl:value-of select="." />
							</xsl:element>		
						</xsl:when>
					</xsl:choose>
				</xsl:for-each>
            </marker>
        </xsl:for-each>
    </markers>
</xsl:template>
</xsl:stylesheet>